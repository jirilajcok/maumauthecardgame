# Mau Mau: The Card Game

Karetní hra Prší jako projekt do PJ1  
*Jiří Lajčok LAJ0013*

## Spuštění

### Konzolová verze

Konzolová verze obsahuje všechny funkce. 

Pro správné zobrazování unicode znaků (barvy karet) je potřeba spustit
se správným kódováním:  
`java Dfile.encoding=UTF-8 MauMauConsole.jar` 

https://bitbucket.org/jirilajcok/maumauthecardgame/downloads/MauMauConsole.jar  
Kód ve větvi `console`

### Grafická verze

V grafické verzi není možné vybrat barvu při změně junkem.
Počítač vybírat barvu může, protože ke změně nepoužívá GUI.

https://bitbucket.org/jirilajcok/maumauthecardgame/downloads/MauMauGUI.jar  
Kód ve větvi `gui`

## Pravidla hry

* Každý hráč začíná se čtyřmi kartami.
* Na kartu o hodnotě 7 protihráč bere 2 karty. Může je však přebíjet jinou sedmou.  
* Na pikového krále se berou 4 karty, možno přebít pouze pikovou sedmou.
* Na eso se stojí, možno přebít jiným esem.
* Konečně, junek mění barvu a může být položen na jakoukoliv jinou kartu.

## Odkazy

Grafika karet ke stažení na stránce https://opengameart.org/content/boardgame-pack
