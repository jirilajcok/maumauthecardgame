package console;

import gameengine.GameEngine;
import gameengine.GameInterface;

public final class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GameInterface game = (new GameEngine.Builder(new ConsoleUserInterface.Builder()))
                .addPlayerLocal("Jirka")
                .addPlayerCPU("Bot")
                .build();

        while (game.turn()) ;
    }

}
