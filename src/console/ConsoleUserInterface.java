package console;

import console.display.HeapDisplay;
import console.display.MePlayerDisplay;
import gameengine.cards.CardInterface;
import gameengine.heap.HeapInterface;
import gameengine.player.MePlayerInterface;
import gameengine.player.PlayerInterface;
import gameengine.rules.RuleReaderInterface;
import gameengine.ui.*;

import java.util.Observable;

public final class ConsoleUserInterface implements GameUserInterface {

    /* References */

    private final HeapDisplay heapDisplay;
    private final MePlayerDisplay mePlayerDisplay;

    /* Initialization */

    ConsoleUserInterface(RuleReaderInterface rules, HeapInterface heap) {
        heapDisplay = new HeapDisplay(rules, heap);
        mePlayerDisplay = new MePlayerDisplay(rules);
    }

    /* User Interfaces */

    private final HeapUserInterface heapUserInterface = new HeapUserInterface() {
        @Override
        public void turn(PlayerInterface player) {
            update(null, null);
        }

        @Override
        public void update(Observable o, Object arg) {
            heapDisplay.update(o, arg);
        }
    };

    private final PlayerUserInterface playerUserInterface = new PlayerUserInterface() {
        @Override
        public void turn(PlayerInterface player) {
            System.out.println(player + "'s turn, has " + player.getNumberOfCards() + " card(s)");
        }

        @Override
        public void cardPut(CardInterface card) {
            System.out.println("Card was put");
        }

        @Override
        public void cardsTaken(int amount) {
            System.out.println("Took " + amount + " card(s)");
        }

        @Override
        public void halted() {
            System.out.println("Halted");
        }

        @Override
        public void update(Observable o, Object arg) {
            if (arg instanceof PlayerInterface)
                turn((PlayerInterface) arg);
            else if (arg instanceof CardInterface)
                cardPut((CardInterface) arg);
            else if (arg instanceof Integer)
                cardsTaken((int) arg);
            else
                halted();
        }
    };

    private final MePlayerUserInterface mePlayerUserInterface = new MePlayerUserInterface() {
        @Override
        public void turn(PlayerInterface player) {
            update(null, player);
        }

        @Override
        public void update(Observable o, Object arg) {
            mePlayerDisplay.update(o, arg);
        }
    };

    @Override
    public HeapUserInterface getHeapUserInterface() {
        return heapUserInterface;
    }

    @Override
    public PlayerUserInterface getPlayerUserInterface() {
        return playerUserInterface;
    }

    @Override
    public MePlayerUserInterface getMePlayerUserInterface() {
        return mePlayerUserInterface;
    }

    /**
     * Console User Interface builder
     */
    public final static class Builder implements GameUserInterfaceBuilder {

        /* Properties */

        private RuleReaderInterface rules;
        private HeapInterface heap;
        private MePlayerInterface mePlayer;

        /* Game User Interface*/

        @Override
        public Builder setRules(RuleReaderInterface rules) {
            this.rules = rules;
            return this;
        }

        @Override
        public Builder setHeap(HeapInterface heap) {
            this.heap = heap;
            return this;
        }

        @Override
        public Builder setMePlayer(MePlayerInterface mePlayer) {
            this.mePlayer = mePlayer;
            return this;
        }

        /* Builder */

        private ConsoleUserInterface consoleUserInterface;

        @Override
        public ConsoleUserInterface build() {
            if (consoleUserInterface == null) {
                if (rules == null || heap == null)
                    throw new IllegalStateException("Rules and heap must be set to build.");
                consoleUserInterface = new ConsoleUserInterface(rules, heap);
                if (mePlayer instanceof Observable)
                    ((Observable) mePlayer).addObserver(consoleUserInterface.getMePlayerUserInterface());
            }
            return consoleUserInterface;
        }
    }
}
