package console.display;

import gameengine.cards.CardInterface;
import gameengine.exceptions.NoSuchCardException;

public final class CardDisplay implements ConsoleDisplayInterface {

    /* Static */

    public static String displaySuit(int suit) {
        if (suit < CardInterface.SUIT_MIN || suit > CardInterface.SUIT_MAX)
            throw new NoSuchCardException("Invalid suit");

        String[] suits = {"\u2663", "\u2660", "\u2665", "\u2666"};
        return suits[suit - 1];
    }

    public static String displayValue(int value) {
        String[] values = {"7", "8", "9", "10", "J", "Q", "K", "A"};
        return  values[value - 7];
    }

    public static String display(CardInterface card) {
        return displaySuit(card.getSuit()) + displayValue(card.getValue());
    }

    /* Properties */

    private final String display;

    /* Initialization */

    public CardDisplay(CardInterface card) {
        this.display = CardDisplay.display(card);
    }

    /* Display */

    @Override
    public String toString() {
        return display;
    }

}
