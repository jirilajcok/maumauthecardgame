package console.display;

import gameengine.cards.CardInterface;
import gameengine.exceptions.IllegalActionException;
import gameengine.exceptions.IllegalGameStateException;
import gameengine.exceptions.NotYourCardException;
import gameengine.player.MePlayerInterface;
import gameengine.rules.RuleReaderInterface;

import java.util.*;

public final class MePlayerDisplay implements ConsoleDisplayInterface, Observer {

    /* Properties */

    private MePlayerInterface player;
    private final RuleReaderInterface rules;

    /* Initialization */

    public MePlayerDisplay(RuleReaderInterface rules) {
        this.rules = rules;
    }

    /* Display */

    @Override
    public String toString() {
        return player.toString();
    }

    private boolean retry = false;

    @Override
    public void display() {
        if (!retry) {
            StringBuilder myCards = new StringBuilder("Your turn:");
            List<CardInterface> cards = player.getCards();
            for (int i = 0; i < cards.size(); i++)
                myCards.append("   ").append(i).append(": ").append(cards.get(i));
            System.out.println(myCards);
        }

        retry = false;

        String enterText = "Enter card's index or x to ";
        boolean stopped = rules.isStopped();
        int take = rules.getCardsTake();
        enterText += stopped ? "halt" : ("take " + take + " card(s)");
        System.out.println(enterText + ":");

        Scanner input = new Scanner(System.in);
        int index;
        try {
            index = input.nextInt();
        } catch (InputMismatchException e) {
            if (!stopped) {
                player.takeCards();
            } else {
                player.halt();
            }
            return;
        }

        CardInterface card;
        try {
            card = player.getCards().get(index);
            player.putCard(card);
        } catch (NotYourCardException | IndexOutOfBoundsException e) {
            retry = true;
            System.out.print("Invalid input, try again:");
            display();
            return;
        } catch (IllegalActionException e) {
            retry = true;
            System.out.println(e.getMessage());
            display();
            return;
        }

        if (card != null && card.equals(CardInterface.JACK))
            chooseSuit();
    }

    private void chooseSuit() {
        Scanner input = new Scanner(System.in);
        StringBuilder suitChoice = new StringBuilder("Choose suit:");
        for (int i = CardInterface.SUIT_MIN; i <= CardInterface.SUIT_MAX; i++)
            suitChoice.append("   ").append(i).append(": ").append(CardDisplay.displaySuit(i));
        System.out.println(suitChoice);

        try {
            int suit = input.nextInt();
            player.setSuitChange(suit);
            System.out.println("Suit has been changed to " + CardDisplay.displaySuit(suit));
        } catch (InputMismatchException | IllegalGameStateException | IllegalArgumentException e) {
            System.out.println("Suit has not been changed, default is used");
        }
    }

    /* Observer */

    @Override
    public void update(Observable o, Object arg) {
        if (player == null) {
            if (o instanceof MePlayerInterface)
                player = (MePlayerInterface) o;
            else if (arg instanceof MePlayerInterface)
                player = (MePlayerInterface) arg;
        }

        if (player == o || player == arg) display();
    }
}