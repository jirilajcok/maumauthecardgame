package console.display;

import gameengine.exceptions.NoCardsRuntimeException;
import gameengine.heap.HeapInterface;
import gameengine.rules.RuleReaderInterface;

import java.util.Observable;
import java.util.Observer;

public final class HeapDisplay implements ConsoleDisplayInterface, Observer {

    /* References */

    private HeapInterface heap;
    private final RuleReaderInterface rules;

    /* Initialization */

    public HeapDisplay(RuleReaderInterface rules, HeapInterface heap) {
        this.rules = rules;
        this.heap = heap;
    }

    /* Display */

    @Override
    public String toString() {
        if (heap == null)
            throw new NoCardsRuntimeException("No card");

        String heapText = "Heap: " + heap.getTopCard();

        int suit = rules.getSuitChange();
        if (suit != 0)
            heapText += ", suit changed to " + CardDisplay.displaySuit(suit);

        return heapText;
    }

    /* Observer */

    @Override
    public void update(Observable o, Object arg) {
        display();
    }
}
