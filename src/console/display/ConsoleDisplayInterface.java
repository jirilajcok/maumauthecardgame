package console.display;

import gameengine.DisplayInterface;

/**
 * Display specifically meant for consoles
 *
 * @author Jiří Lajčok
 */
public interface ConsoleDisplayInterface extends DisplayInterface {

    String toString();

    @Override
    default void display() {
        System.out.println(this);
    }
}
