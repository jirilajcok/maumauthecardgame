package enhance;

import java.util.Iterator;

public interface IteratorCurrent<E> extends Iterator<E> {

    /**
     * Determines if there is current element
     *
     * @return Returns whether there an element currently pointed on
     */
    boolean hasCurrent();

    /**
     * Gets current element without moving the pointer
     *
     * @return Returns current element
     */
    E current();

}
