package gameengine;

/**
 * Represents game display of any kind
 *
 * @author Jiří Lajčok
 */
public interface DisplayInterface {

    /**
     * Basic display method to show changes
     */
    default void display() {}

}
