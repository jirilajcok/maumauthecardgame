package gameengine.cards;

import gameengine.exceptions.NoCardsException;

import java.util.List;

/**
 * Represents card pack.<br>
 * The class provides only stated amount of cards. More cannot be obtained.
 *
 * @author Jiří Lajčok
 */
public interface CardPackInterface {

    /**
     * Gets rest of the cards in random order. They can be obtained only once.
     *
     * @return Returns all the cards in a List
     * @throws NoCardsException In case the pack is empty
     */
    List<CardInterface> getCards() throws NoCardsException;

    /**
     * Takes specified amount of cards from the pack.
     *
     * @param amount Amount to take
     * @return Returns list of taken cards
     * @throws NoCardsException In case the pack is empty or there is not that much cards in it
     */
    List<CardInterface> getCards(int amount) throws NoCardsException;

    /**
     * Gets one card. Might be random and card do not repeat.
     *
     * @return Returns one Card or null pointer if empty
     */
    CardInterface getNextCard();

}
