package gameengine.cards;

/**
 * Represents one card
 * TODO: Add card proxy (not playable, only to see), if there is time
 *
 * @author Jiří Lajčok
 */
public interface CardInterface {

    int SUIT_ACORNS = 1;
    int SUIT_LEAVES = 2;
    int SUIT_HEARTS = 3;
    int SUIT_BELLS = 4;

    int SUIT_MIN = SUIT_ACORNS;
    int SUIT_MAX = SUIT_BELLS;

    /**
     * Get the suit of the card
     *
     * @return Returns one of the suit constants: SUIT_ACORNS, SUIT_LEAVES, SUIT_HEARTS or SUIT_BELLS
     */
    int getSuit();

    int SEVEN = 7;
    int EIGHT = 8;
    int NINE = 9;
    int TEN = 10;
    int JACK = 11;
    int QUEEN = 12;
    int KING = 13;
    int ACE = 14;

    /**
     * Get the value of the card. Number card correspond with the int value,
     * others follow the number line. Every possible value also has a class constant.
     *
     * @return Returns the value: SEVEN up to ACE
     */
    int getValue();

    /**
     * Find out whether card is asked suit or value
     * @param suitOrValue Card suit or value
     * @return Returns whether checks
     */
    boolean equals(int suitOrValue);

    /**
     * Find out whether card is asked suit and value
     * @param suit Card suit - one of the constants
     * @param value Card value, can be numeric or one of the constants
     * @return Returns whether checks
     */
    boolean equals(int suit, int value);

    /**
     * Compares a card with another one
     * @param card Another card to compare to
     * @return Returns whether checks
     */
    boolean equals(CardInterface card);

}
