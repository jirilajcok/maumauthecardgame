package gameengine.cards;

import console.display.CardDisplay;
import console.display.ConsoleDisplayInterface;
import gameengine.exceptions.NoCardsException;
import gameengine.exceptions.NoSuchCardException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * Represents 32 cards pack
 *
 * @author Jiří Lajčok
 */
public final class CardPack implements CardPackInterface {

    /**
     * One particular card
     */
    private class Card implements CardInterface, ConsoleDisplayInterface {

        /* Properties */

        private final int s;

        @Override
        public int getSuit() {
            return s;
        }

        private final int v;

        @Override
        public int getValue() {
            return v;
        }

        @Override
        public boolean equals(int suitOrValue) {
            return s == suitOrValue || v == suitOrValue;
        }

        @Override
        public boolean equals(int suit, int value) {
            return s == suit && v == value;
        }

        @Override
        public boolean equals(CardInterface card) {
            return s == card.getSuit() && v == card.getValue();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof CardInterface)
                return equals((CardInterface) obj);
            else if (obj instanceof Integer)
                return equals((int) obj);
            return false;
        }

        /* Initialization */

        /**
         * Constructor based on suit and value
         *
         * @throws NoSuchCardException In case card does not exist
         */
        Card(int suit, int value) throws NoSuchCardException {
            if (suit < SUIT_MIN || suit > SUIT_MAX)
                throw new NoSuchCardException("Given suite is out of range, a valid suite constant must be used");
            if (value < SEVEN || value > ACE)
                throw new NoSuchCardException("Given card value is out of range, a valid value must be used");

            s = suit;
            v = value;
        }

        /* Console */

        private ConsoleDisplayInterface display;

        @Override
        public String toString() {
            if (display == null)
                display = new CardDisplay(this);
            return display.toString();
        }
    }

    /* Properties */

    private final Stack<CardInterface> cards = new Stack<>();

    private boolean generated = false;

    private void generate() {
        if (generated) return;

        for (int suit = Card.SUIT_MIN; suit <= Card.SUIT_MAX; suit++)
            for (int value = Card.SEVEN; value <= Card.ACE; value++)
                try {
                    cards.add(new Card(suit, value));
                } catch (NoSuchCardException e) {
                    e.printStackTrace();
                }

        Collections.shuffle(cards);

        generated = true;
    }

    /* Card Pack */

    @Override
    public List<CardInterface> getCards() throws NoCardsException {
        generate();
        if (cards.empty())
            throw new NoCardsException("You took all of me, right? I have nothing left so leave me alone!");

        List<CardInterface> result = new ArrayList<>(cards);
        cards.clear();
        return result;
    }

    @Override
    public List<CardInterface> getCards(int amount) throws NoCardsException {
        generate();
        int size = cards.size();
        if (amount > size)
            throw new NoCardsException("There is not enough cards to get an amount of " + amount + ".");

        List<CardInterface> end = cards.subList(size - amount, size);
        List<CardInterface> res = new ArrayList<>(end);
        end.clear();
        return res;
    }

    @Override
    public CardInterface getNextCard() {
        generate();
        return !cards.empty() ? cards.pop() : null;
    }
}
