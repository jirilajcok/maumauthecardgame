package gameengine;

import gameengine.exceptions.InvalidPlayersNumberException;
import javafx.util.Builder;

public interface GameBuilderInterface extends Builder<GameInterface> {

    /**
     * Adds local player controller to game
     * @param nick Nick of new player
     * @return Returns self
     */
    GameBuilderInterface addPlayerLocal(String nick);

    /**
     * Adds CPU player controller to game
     * @param nick Nick of new player
     * @return Returns self
     */
    GameBuilderInterface addPlayerCPU(String nick);

    /**
     * Builds a game from given parts
     *
     * @return Returns a new game or one already built
     * @throws InvalidPlayersNumberException In case not enough players have been added
     */
    GameInterface build();

}
