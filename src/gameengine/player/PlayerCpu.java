package gameengine.player;

import gameengine.cards.CardInterface;
import gameengine.rules.RuleReaderInterface;

import java.util.Observable;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This player controller implementation represents a computer (CPU) player which is controlled automatically.
 *
 * @author Jiří Lajčok
 */
public final class PlayerCpu extends Observable implements PlayerControllerInterface {

    /* References */

    private final MePlayerInterface player;
    private final RuleReaderInterface rules;

    /* Initialization */

    public PlayerCpu(MePlayerInterface player, RuleReaderInterface rules) {
        this.player = player;
        this.rules = rules;
    }

    /* Controller */

    private PlayerInterface cache;

    @Override
    public PlayerInterface getPlayer() {
        if (cache == null)
            cache = new PlayerProxy(player);
        return cache;
    }

    @Override
    public void play() {
        System.out.println("CPU player turn");

        setChanged();
        notifyObservers(getPlayer());

        for (CardInterface card : player.getCards())
            if (rules.canPutCard(card)) {
                player.putCard(card);
                chooseSuit(card);
                return;
            }

        if (rules.isStopped()) {
            player.halt();
            return;
        }

        player.takeCards();
    }

    private int chooseSuit(CardInterface card) {
        if (!card.equals(CardInterface.JACK))
            return 0;

        ThreadLocalRandom random = ThreadLocalRandom.current();
        int suit = random.nextInt(CardInterface.SUIT_MIN, CardInterface.SUIT_MAX + 1);

        player.setSuitChange(suit);

        return suit;
    }
}
