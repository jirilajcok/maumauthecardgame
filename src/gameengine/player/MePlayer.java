package gameengine.player;

import gameengine.cards.CardInterface;
import gameengine.exceptions.IllegalActionException;
import gameengine.exceptions.IllegalGameStateException;
import gameengine.exceptions.NoCardsException;
import gameengine.exceptions.NotYourCardException;
import gameengine.heap.HeapInterface;
import gameengine.rules.RuleInterface;
import gameengine.rules.RuleReaderInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

/**
 * Implementation of a me or my player whose behavior can be controlled by this.
 *
 * @author Jiří Lajčok
 */
public final class MePlayer extends Observable implements MePlayerInterface {

    /* References */

    private final HeapInterface heap;
    private final RuleInterface rules;

    /* Properties */

    private final String nick;
    private final List<CardInterface> cards;

    /* Initialization */

    private MePlayer(Builder builder) {
        heap = builder.heap;
        rules = builder.rules;
        nick = builder.nick;
        cards = builder.cards;
    }

    /* Player */

    @Override
    public String getNick() {
        return nick;
    }

    @Override
    public String toString() {
        return getNick();
    }

    @Override
    public int getNumberOfCards() {
        return cards.size();
    }

    @Override
    public boolean hasWon() {
        return RuleReaderInterface.hasWon(this);
    }

    private void checkWon() {
        if (hasWon()) throw new IllegalGameStateException("Hey " + this + ", you're outta game already!");
    }

    @Override
    public List<CardInterface> getCards() {
        return new ArrayList<>(cards);
    }

    @Override
    public void takeCards() {
        System.out.println("TAKE");
        checkWon();
        if (rules.isStopped())
            throw new IllegalGameStateException("You can't take cards right meow. You are stopped.");

        int take = rules.getCardsTake();
        for (int i = 0; i < take; i++)
            try {
                cards.add(heap.takeCard());
            } catch (NoCardsException e) {
                break;
            }

        setChanged();
        notifyObservers(take);
    }

    @Override
    public void halt() {
        System.out.println("HALT");
        checkWon();
        if (!rules.isStopped())
            throw new IllegalGameStateException("Why would you halt?");

        setChanged();
        notifyObservers("h");
    }

    @Override
    public void putCard(CardInterface card) {
        System.out.println("PUT " + card);
        checkWon();
        if (!cards.contains(card))
            throw new NotYourCardException("Hey man, where'd you take that?!");

        if (!rules.canPutCard(card))
            if (rules.isStopped())
                throw new IllegalActionException("Use ACE or DIE! (or just halt)");
            else if (rules.getCardsTake() > 1)
                throw new IllegalActionException("You should consider fighting back or just taking the cards.");
            else
                throw new IllegalActionException("WRONG! Learn some rules, ok?");

        heap.putCard(card);
        cards.remove(card);

        setChanged();
        notifyObservers(card);
    }

    @Override
    public void putCard(int index) {
        try {
            putCard(cards.get(index));
        } catch (IndexOutOfBoundsException e) {
            throw new NotYourCardException("Invalid index.");
        }
    }

    @Override
    public void setSuitChange(int suit) {
        System.out.println("SUIT CHANGE " + suit);
        rules.setSuitChange(suit);
        setChanged();
        notifyObservers("s");
    }

    /**
     * Player builder class creates a player with everything he needs.<br>
     * Only use this to create one player.<br>
     * Also can represent a player.
     *
     * @author Jiří Lajčok
     */
    public static final class Builder implements PlayerBuilderInterface, PlayerInterface {

        /* Properties */

        private String nick;
        private List<CardInterface> cards;

        /* References */

        private HeapInterface heap;
        private RuleInterface rules;

        /* Initialization */

        /**
         * Initiate builder with a nick
         *
         * @param nick Nick name
         */
        Builder(String nick) {
            this.nick = nick;
        }

        /* Player */

        @Override
        public String getNick() {
            if (player != null) return player.getNick();
            return nick;
        }

        @Override
        public Builder setNick(String nick) {
            this.nick = nick;
            return this;
        }

        @Override
        public String toString() {
            if (player != null) return player.toString();
            return nick;
        }

        @Override
        public int getNumberOfCards() {
            if (player != null) return player.getNumberOfCards();
            return cards != null ? cards.size() : 0;
        }

        @Override
        public boolean hasWon() {
            if (player != null) return player.hasWon();
            if (cards == null)
                throw new IllegalStateException("Cannot check unless cards are assigned.");
            return RuleReaderInterface.hasWon(this);
        }

        /* Builder */

        @Override
        public Builder setHeap(HeapInterface heap) {
            this.heap = heap;
            return this;
        }

        @Override
        public Builder setRules(RuleInterface rules) {
            this.rules = rules;
            return this;
        }

        @Override
        public Builder setCards(List<CardInterface> cards) {
            this.cards = new ArrayList<>(cards);
            return this;
        }

        @Override
        public Builder addCard(CardInterface... cards) {
            List<CardInterface> insert = Arrays.asList(cards);
            if (insert.isEmpty())
                return this;

            if (this.cards == null)
                this.cards = new ArrayList<>(insert);
            else
                this.cards.addAll(insert);
            return this;
        }

        /* Builder */

        private MePlayer player;

        @Override
        public MePlayer build() {
            if (player != null) return player;

            if (nick == null || nick.isEmpty())
                throw new IllegalStateException("Excuse me, set up some nick, sir.");
            if (cards.isEmpty())
                throw new IllegalStateException("Hey, what about my cards?!");

            return player = new MePlayer(this);
        }

    }
}
