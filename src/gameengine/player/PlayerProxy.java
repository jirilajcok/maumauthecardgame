package gameengine.player;

/**
 * Implementation of player interface which disables access player as 'me player'
 *
 * @author Jiří Lajčok
 */
public final class PlayerProxy implements PlayerInterface {

    /* Properties */

    private final PlayerInterface player;

    /* Initialization */

    public PlayerProxy(PlayerInterface player) {
        this.player = player;
    }

    /* Player */

    @Override
    public String getNick() {
        return player.getNick();
    }

    @Override
    public String toString() {
        return player.getNick();
    }

    @Override
    public int getNumberOfCards() {
        return player.getNumberOfCards();
    }

    @Override
    public boolean hasWon() {
        return player.hasWon();
    }
}
