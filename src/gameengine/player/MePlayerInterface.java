package gameengine.player;

import gameengine.cards.CardInterface;
import gameengine.exceptions.IllegalGameStateException;
import gameengine.exceptions.NotYourCardException;

import java.util.List;

/**
 * Represents a player on current machine, a player that we play for
 *
 * @author Jiří Lajčok
 */
public interface MePlayerInterface extends PlayerInterface {

    /**
     * Get player's cards
     *
     * @return Returns a list of the player's (my) cards
     */
    List<CardInterface> getCards();

    /**
     * Takes card(s) from heap (required amount)
     *
     * @throws IllegalGameStateException In case action is not expected at the moment,
     *                                   e.g. game is stopped
     * @throws IllegalGameStateException In case has already won
     */
    void takeCards();

    /**
     * Halt, e.g. on ACE
     *
     * @throws IllegalGameStateException In case action is not expected at the moment,
     *                                   e.g. game not stopped
     * @throws IllegalGameStateException In case has already won
     */
    void halt();

    /**
     * Put card onto heap
     *
     * @param card Card to be put
     * @throws NotYourCardException      In case the card is not in player's hand
     *                                   (do not know where he got it)
     * @throws IllegalGameStateException In case action is not expected at the moment,
     *                                   e.g. invalid card is being used (when stopped, offended by cards take)
     * @throws IllegalGameStateException In case has already won
     */
    void putCard(CardInterface card);

    /**
     * Put card onto heap
     *
     * @param index Index of player's card in series
     * @throws NotYourCardException      In case the index does not represent any card in player's hand
     * @throws IllegalGameStateException In case action is not expected at the moment,
     *                                   e.g. invalid card is being used (when stopped, offended by cards take)
     * @throws IllegalGameStateException In case has already won
     */
    void putCard(int index);

    /**
     * Sets suit change
     *
     * @param suit Suit to change to
     * @throws IllegalGameStateException In case suit change is not expected
     */
    void setSuitChange(int suit);

}
