package gameengine.player;

import gameengine.rules.RuleReaderInterface;

import java.util.Observable;

/**
 * This player controller implementation represents a local physical player using console input.
 *
 * @author Jiří Lajčok
 */
public final class PlayerLocal extends Observable implements PlayerControllerInterface {

    /* References */

    private final MePlayerInterface player;
    private final RuleReaderInterface rules;

    /* Initialization */

    public PlayerLocal(MePlayerInterface player, RuleReaderInterface rules) {
        this.player = player;
        this.rules = rules;
    }

    /* Controller */

    private PlayerInterface proxy;

    @Override
    public PlayerInterface getPlayer() {
        if (proxy == null)
            proxy = new PlayerProxy(player);
        return proxy;
    }

    @Override
    public void play() {
        System.out.println("Local player turn");
        setChanged();
        notifyObservers(player);
    }

}
