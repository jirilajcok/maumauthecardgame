package gameengine.player;

import gameengine.cards.CardInterface;
import gameengine.heap.HeapInterface;
import gameengine.rules.RuleInterface;
import javafx.util.Builder;

import java.util.List;

/**
 * Player builder class creates a player with everything he needs.
 * Only use this to create one player.
 *
 * @author Jiří Lajčok
 */
public interface PlayerBuilderInterface extends Builder {

    /**
     * Sets nick name
     *
     * @param nick Nick name to set
     * @return Returns self
     */
    PlayerBuilderInterface setNick(String nick);

    /**
     * Sets heap to play with
     *
     * @param heap A heap (mandatory)
     * @return Returns self
     */
    PlayerBuilderInterface setHeap(HeapInterface heap);

    /**
     * Sets rules to use
     *
     * @param rules Rules (optional if adding complete player controllers)
     * @return Returns self
     */
    PlayerBuilderInterface setRules(RuleInterface rules);

    /**
     * Sets player's cards, overrides previously existing ones
     *
     * @param cards Cards to set
     * @return Returns self
     */
    PlayerBuilderInterface setCards(List<CardInterface> cards);

    /**
     * Adds cards to player's hand
     *
     * @param cards   Cards to add
     * @return Returns self
     */
    PlayerBuilderInterface addCard(CardInterface... cards);

    /**
     * Builds a player based on given attributes
     *
     * @return Returns a new player or previously built one
     * @throws IllegalStateException If any of attributes has not been set
     */
    MePlayerInterface build();

}
