package gameengine.player;

import gameengine.cards.CardInterface;
import gameengine.cards.CardPackInterface;
import gameengine.exceptions.NoCardsException;
import gameengine.exceptions.NoCardsRuntimeException;
import gameengine.heap.HeapInterface;
import gameengine.rules.RuleInterface;
import gameengine.rules.RuleProxy;
import gameengine.rules.RuleReaderInterface;
import gameengine.ui.UserInterface;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Observer;

public class PlayerFactory implements PlayerFactoryInterface {

    /* References */

    private final RuleInterface rules;
    private final RuleReaderInterface rulesProxy;
    private final HeapInterface heap;
    private final CardPackInterface cardPack;

    /* Initialization */

    public PlayerFactory(RuleInterface rules, HeapInterface heap, CardPackInterface cardPack) {
        this.rules = rules;
        this.heap = heap;
        this.cardPack = cardPack;
        rulesProxy = new RuleProxy(rules);
    }

    /* Factory */

    @Override
    public PlayerControllerInterface create(String nick, Class<? extends PlayerControllerInterface> type) {
        return create(nick, type, null);
    }

    @Override
    public PlayerControllerInterface create(String nick, Class<? extends PlayerControllerInterface> type, UserInterface playerObserver) {
        List<CardInterface> cards;
        try {
            cards = cardPack.getCards(RuleReaderInterface.PLAYER_CARDS);
        } catch (NoCardsException e) {
            throw new NoCardsRuntimeException(e.getMessage());
        }

        MePlayer player = (new MePlayer.Builder(nick))
                .setRules(rules)
                .setHeap(heap)
                .setCards(cards)
                .build();

        if (rules instanceof Observer)
            player.addObserver((Observer) rules);

        if (playerObserver != null) {
            player.addObserver(playerObserver);
            playerObserver.update(player, null);
        }

        try {
            return type
                    .getDeclaredConstructor(
                            MePlayerInterface.class,
                            RuleReaderInterface.class
                    )
                    .newInstance(player, rulesProxy);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }
}
