package gameengine.player;

/**
 * Represents general player, his properties and actions
 *
 * @author Jiří Lajčok
 */
public interface PlayerInterface {

    /**
     * Gets player's nick name
     *
     * @return Player's nick name
     */
    String getNick();

    /**
     * Converts object to string as player's nick name
     *
     * @return Player's nick name
     */
    String toString();

    /**
     * Get number of player's cards
     *
     * @return Returns number of cards
     */
    int getNumberOfCards();

    /**
     * Determines whether player has won - has no cards left
     *
     * @return Returns whether player has won
     */
    boolean hasWon();
}
