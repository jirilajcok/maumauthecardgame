package gameengine.player;

import gameengine.ui.UserInterface;

/**
 * Player CONTROLLER factory, simplifies the creation process
 *
 * @author Jiří Lajčok
 */
public interface PlayerFactoryInterface {

    /**
     * Creates a new player controller.
     *
     * @param type Class whose instance is meant to be created (must have compatible constructor)
     *             with parameters MePlayerInterface and RuleReaderInterface
     * @return Returns a new player controller
     */
    PlayerControllerInterface create(String nick, Class<? extends PlayerControllerInterface> type);

    /**
     * Creates a new player controller with observer to otherwise inaccessible player attributes.
     *
     * @param nick             Nick to set a player
     * @param type             Class whose instance is meant to be created (must have compatible constructor)
     *                         with parameters MePlayerInterface and RuleReaderInterface
     * @param playerObserver Observer added to the me player inside controller
     * @return Returns a new player controller
     */
    PlayerControllerInterface create(String nick, Class<? extends PlayerControllerInterface> type, UserInterface playerObserver);

}
