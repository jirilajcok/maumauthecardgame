package gameengine.player;

/**
 * Controls a player in a game.
 *
 * @author Jiří Lajčok
 */
public interface PlayerControllerInterface {

    /**
     * Gets controlled player
     *
     * @return Returns general player
     */
    PlayerInterface getPlayer();

    /**
     * Makes player decide and make a move
     */
    void play();

}
