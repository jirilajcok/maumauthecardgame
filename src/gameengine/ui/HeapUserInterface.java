package gameengine.ui;

import gameengine.cards.CardInterface;

import java.util.Observable;

/**
 * Represents heap user interface which is updated whenever an action is triggered.
 *
 * @author Jiří Lajčok
 */
public interface HeapUserInterface extends UserInterface {

    /**
     * Triggered whenever a card is put on top
     *
     * @param card Card that has been put
     */
    default void cardPut(CardInterface card) {
    }

    /**
     * Triggered whenever a card is taken from heap
     */
    default void cardTaken() {
    }

    /**
     * Depending on sender and argument, MAIN communication with game
     *
     * @param o   If GameInterface, game is turned.
     *            If heap means change in heap.
     * @param arg If heap, null means card taken and a card means card put
     */
    @Override
    default void update(Observable o, Object arg) {
    }
}
