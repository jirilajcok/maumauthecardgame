package gameengine.ui;

import java.util.Observable;

/**
 * Observes whenever a player's activity is demanded
 *
 * @author Jiří Lajčok
 */
public interface MePlayerUserInterface extends PlayerUserInterface {

    /**
     * Player chooses suit
     */
    default void chooseSuit() {
    }

    /**
     * Depends on argument
     *
     * @param o   A player or a game.
     * @param arg If a player, means turn. Choose suit otherwise.
     */
    @Override
    default void update(Observable o, Object arg) {
    }
}
