package gameengine.ui;

import gameengine.player.PlayerInterface;

import java.util.Observable;
import java.util.Observer;

public interface UserInterface extends Observer {

    /**
     * Triggered whenever turn is initiated
     *
     * @param player Player on turn
     */
    default void turn(PlayerInterface player) {
    }

    /**
     * Update method function varies depending on user interface controller type
     *
     * @param o   Observed object
     * @param arg Additional argument
     */
    @Override
    default void update(Observable o, Object arg) {
    }
}
