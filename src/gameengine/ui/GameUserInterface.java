package gameengine.ui;

public interface GameUserInterface {

    HeapUserInterface getHeapUserInterface();

    PlayerUserInterface getPlayerUserInterface();

    MePlayerUserInterface getMePlayerUserInterface();

}
