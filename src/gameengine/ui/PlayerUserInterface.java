package gameengine.ui;

import gameengine.cards.CardInterface;

import java.util.Observable;

/**
 * Player observer which displays changes on general player.
 *
 * @author Jiří Lajčok
 */
public interface PlayerUserInterface extends UserInterface {

    /**
     * Whenever a card is put on heap
     *
     * @param card Card that has been put
     */
    default void cardPut(CardInterface card) {
    }

    /**
     * Player took given number of cards
     *
     * @param amount Number of cards taken
     */
    default void cardsTaken(int amount) {
    }

    /**
     * Player halted
     */
    default void halted() {
    }

    /**
     * Player has chosen suit
     */
    default void suitChosen(int suit) {
    }

    /**
     * Depends on arguments
     *
     * @param o   If a game, turn has been made, otherwise it should be its player
     * @param arg Card that has been put, number of cards that has been taken or null if halted
     */
    @Override
    default void update(Observable o, Object arg) {
    }
}
