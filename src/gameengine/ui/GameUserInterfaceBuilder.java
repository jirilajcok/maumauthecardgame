package gameengine.ui;

import gameengine.GameBuilderInterface;
import gameengine.heap.HeapInterface;
import gameengine.player.MePlayerInterface;
import gameengine.rules.RuleReaderInterface;
import javafx.util.Builder;

public interface GameUserInterfaceBuilder extends Builder<GameUserInterface> {

    GameUserInterfaceBuilder setRules(RuleReaderInterface rules);

    GameUserInterfaceBuilder setHeap(HeapInterface heap);

    GameUserInterfaceBuilder setMePlayer(MePlayerInterface mePlayer);

    default GameUserInterfaceBuilder setGameBuilder(GameBuilderInterface gameBuilder) {
        return this;
    }

}
