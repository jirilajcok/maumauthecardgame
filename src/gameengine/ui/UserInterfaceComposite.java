package gameengine.ui;

import gameengine.heap.HeapInterface;
import gameengine.player.MePlayerInterface;
import gameengine.rules.RuleReaderInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

public final class UserInterfaceComposite implements GameUserInterface {

    /* Properties */

    private final List<GameUserInterface> userInterfaces;

    /* Initialization */

    private UserInterfaceComposite(List<GameUserInterface> userInterfaces) {
        this.userInterfaces = userInterfaces;
    }

    /* User Interface */

    private HeapUserInterface heapUI = new HeapUserInterface() {
        @Override
        public void update(Observable o, Object arg) {
            userInterfaces.forEach(ui -> ui.getHeapUserInterface().update(o, arg));
        }
    };

    private PlayerUserInterface playerUI = new PlayerUserInterface() {
        @Override
        public void update(Observable o, Object arg) {
            userInterfaces.forEach(ui -> ui.getPlayerUserInterface().update(o, arg));
        }
    };

    private MePlayerUserInterface mePlayerUI = new MePlayerUserInterface() {
        @Override
        public void update(Observable o, Object arg) {
            userInterfaces.forEach(ui -> ui.getMePlayerUserInterface().update(o, arg));
        }
    };

    @Override
    public HeapUserInterface getHeapUserInterface() {
        return heapUI;
    }

    @Override
    public PlayerUserInterface getPlayerUserInterface() {
        return playerUI;
    }

    @Override
    public MePlayerUserInterface getMePlayerUserInterface() {
        return mePlayerUI;
    }

    public final static class Builder implements GameUserInterfaceBuilder {

        private final List<GameUserInterfaceBuilder> uiBuilders;

        public Builder(GameUserInterfaceBuilder... uiBuilders) {
            this.uiBuilders = Arrays.asList(uiBuilders);
        }

        @Override
        public GameUserInterfaceBuilder setRules(RuleReaderInterface rules) {
            uiBuilders.forEach(ui -> ui.setRules(rules));
            return this;
        }

        @Override
        public GameUserInterfaceBuilder setHeap(HeapInterface heap) {
            uiBuilders.forEach(ui -> ui.setHeap(heap));
            return this;
        }

        @Override
        public GameUserInterfaceBuilder setMePlayer(MePlayerInterface mePlayer) {
            uiBuilders.forEach(ui -> ui.setMePlayer(mePlayer));
            return this;
        }

        @Override
        public GameUserInterface build() {
            List<GameUserInterface> userInterfaces = new ArrayList<>(uiBuilders.size());
            for (GameUserInterfaceBuilder builder : uiBuilders)
                userInterfaces.add(builder.build());
            return new UserInterfaceComposite(userInterfaces);
        }
    }

}
