package gameengine;

import gameengine.player.PlayerInterface;

import java.util.List;

public interface GameImmutableInterface {

    /* Constants */

    int MIN_PLAYERS = 2;
    int MAX_PLAYERS = 4;

    /* Game */

    /**
     * Gets players participating in the game
     *
     * @return Returns list of players
     */
    List<PlayerInterface> getPlayers();

    /**
     * Gets the player currently playing, on turn
     *
     * @return Returns current player
     */
    PlayerInterface getCurrent();

    /**
     * Gets players that has already won in order from the first winner to the last
     *
     * @return Returns list of winners in order
     */
    List<PlayerInterface> getWinners();

}
