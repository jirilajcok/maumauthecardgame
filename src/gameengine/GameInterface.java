package gameengine;

/**
 * Represents game in terms of sticking its parts together and organizes game by circulating between players.
 *
 * @author Jiří Lajčok
 */
public interface GameInterface extends GameImmutableInterface {

    /* Game */

    /**
     * Starts game, triggers next player's turn
     *
     * @return Returns true if the game pends or false otherwise (game over).
     */
    boolean turn();

}
