package gameengine.exceptions;

/**
 * Should be thrown if there is a duplicate card where it should not be
 *
 * @author Jiří Lajčok
 */
public final class DuplicateCardException extends GameRuntimeException {

    public DuplicateCardException() { super(); }

    public DuplicateCardException(String message) { super(message); }

}
