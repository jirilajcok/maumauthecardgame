package gameengine.exceptions;

/**
 * This exception should be thrown if there are not any more cards where it should be, this is an error
 *
 * @author Jiří Lajčok
 */
public final class NoCardsRuntimeException extends GameRuntimeException {

    public NoCardsRuntimeException() {
        super();
    }

    public NoCardsRuntimeException(String message) {
        super(message);
    }

}
