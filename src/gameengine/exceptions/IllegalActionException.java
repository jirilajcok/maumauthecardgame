package gameengine.exceptions;

public final class IllegalActionException extends GameRuntimeException {

    public IllegalActionException(String message) {
        super(message);
    }

}
