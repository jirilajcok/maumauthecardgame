package gameengine.exceptions;

/**
 * Should be thrown when an operation is requested in wrong context
 *
 * @author Jiří Lajčok
 */
public final class IllegalGameStateException extends GameRuntimeException {

    public IllegalGameStateException() {
        super();
    }

    public IllegalGameStateException(String message) {
        super(message);
    }

}
