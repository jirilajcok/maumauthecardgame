package gameengine.exceptions;

/**
 * The exception should be thrown in case non-existing card is being factored
 *
 * @author Jiří Lajčok
 */
public final class NoSuchCardException extends GameRuntimeException {

    public NoSuchCardException() {
        super();
    }

    public NoSuchCardException(String message) {
        super(message);
    }

}
