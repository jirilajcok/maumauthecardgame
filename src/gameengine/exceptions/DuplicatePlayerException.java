package gameengine.exceptions;

/**
 * Should be thrown if there is already this named player or even the same is added to game
 *
 * @author Jiří Lajčok
 */
public final class DuplicatePlayerException extends GameRuntimeException {

    public DuplicatePlayerException(String message) {
        super(message);
    }

}
