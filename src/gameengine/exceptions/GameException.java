package gameengine.exceptions;

/**
 * This is top level caught for all in-game-thrown exceptions
 *
 * @author Jiří Lajčok
 */
class GameException extends Exception {

    GameException() {
        super();
    }

    GameException(String message) {
        super(message);
    }

}
