package gameengine.exceptions;

/**
 * This exception should be thrown if there are not any more cards because they have been taken
 *
 * @author Jiří Lajčok
 */
public final class NoCardsException extends GameException {

    public NoCardsException() {
        super();
    }

    public NoCardsException(String message) {
        super(message);
    }

}
