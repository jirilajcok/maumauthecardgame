package gameengine.exceptions;

/**
 * To be thrown if player tries to put a card he does not have in his hand.
 *
 * @author Jiří Lajčok
 */
public final class NotYourCardException extends GameRuntimeException {

    public NotYourCardException(String message) {
        super(message);
    }

}
