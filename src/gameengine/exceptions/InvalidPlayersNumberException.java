package gameengine.exceptions;

/**
 * Use this exception to express too little / many players in game
 *
 * @author Jiří Lajčok
 */
public final class InvalidPlayersNumberException extends GameRuntimeException {

    public InvalidPlayersNumberException(String message) {
        super(message);
    }

}
