package gameengine.exceptions;

/**
 * This is top level uncaught for all in-game-thrown exceptions
 *
 * @author Jiří Lajčok
 */
class GameRuntimeException extends RuntimeException {

    GameRuntimeException() {
        super();
    }

    GameRuntimeException(String message) {
        super(message);
    }

}
