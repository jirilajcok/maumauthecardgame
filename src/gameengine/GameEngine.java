package gameengine;

import enhance.IteratorCurrent;
import gameengine.cards.CardPack;
import gameengine.cards.CardPackInterface;
import gameengine.exceptions.DuplicatePlayerException;
import gameengine.exceptions.InvalidPlayersNumberException;
import gameengine.heap.HeapBuilder;
import gameengine.heap.HeapBuilderInterface;
import gameengine.heap.HeapInterface;
import gameengine.heap.HeapProxy;
import gameengine.player.*;
import gameengine.rules.RuleController;
import gameengine.rules.RuleProxy;
import gameengine.rules.RuleReaderInterface;
import gameengine.ui.GameUserInterface;
import gameengine.ui.GameUserInterfaceBuilder;
import gameengine.ui.MePlayerUserInterface;
import gameengine.ui.PlayerUserInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Implementation of game interface, turns are observable.
 *
 * @author Jiří Lajčok
 * @see GameInterface
 */
public final class GameEngine extends Observable implements GameInterface {

    /* Properties */

    private final List<PlayerControllerInterface> players;
    private final List<PlayerControllerInterface> winners;

    /**
     * Infinite iterator for players
     */
    private final IteratorCurrent<PlayerControllerInterface> iterator = new IteratorCurrent<PlayerControllerInterface>() {
        private int index = -1;

        @Override
        public boolean hasNext() {
            return players.size() > 1;
        }

        @Override
        public PlayerControllerInterface next() {
            return players.get(++index % players.size());
        }

        @Override
        public boolean hasCurrent() {
            return !players.isEmpty();
        }

        @Override
        public PlayerControllerInterface current() {
            return players.get((index >= 0 ? index : 0) % players.size());
        }

        @Override
        public void remove() {
            players.remove(index % players.size());
        }
    };

    /* Initialization */

    public GameEngine(List<PlayerControllerInterface> players) {
        this.players = players;
        winners = new ArrayList<>(players.size());
    }

    /* Game */

    @Override
    public List<PlayerInterface> getPlayers() {
        List<PlayerInterface> res = new ArrayList<>(players.size());
        for (PlayerControllerInterface player : players)
            res.add(player.getPlayer());
        return res;
    }

    @Override
    public List<PlayerInterface> getWinners() {
        List<PlayerInterface> res = new ArrayList<>(winners.size());
        for (PlayerControllerInterface winner : winners)
            res.add(winner.getPlayer());
        return res;
    }

    @Override
    public PlayerInterface getCurrent() {
        return iterator.current().getPlayer();
    }

    @Override
    public boolean turn() {
        PlayerControllerInterface player = iterator.next();
        if (!iterator.hasNext()) {
            iterator.remove();
            winners.add(player);
            return false;
        }

        System.out.println("GAME TURN");

        player.play();

        setChanged();
        notifyObservers(player.getPlayer());

        if (player.getPlayer().hasWon()) {
            iterator.remove();
            winners.add(player);
        }

        return true;
    }

    public static final class Builder implements GameBuilderInterface {

        /* References */

        private final List<PlayerControllerInterface> players = new ArrayList<>(MIN_PLAYERS);
        private final PlayerFactoryInterface playerFactory;

        private final HeapBuilderInterface heap;
        private final GameUserInterface gameUI;

        /* Initialization */

        public Builder(GameUserInterfaceBuilder uiBuilder) {
            CardPackInterface cardPack = new CardPack();
            HeapBuilder heapBuilder = new HeapBuilder(cardPack);

            RuleController ruleController = new RuleController();
            heapBuilder.addObserver(ruleController);

            RuleReaderInterface rules = new RuleProxy(ruleController);
            HeapInterface heapProxy = new HeapProxy(heapBuilder);

            heap = heapBuilder;
            gameUI = uiBuilder.setRules(rules).setHeap(heapProxy).setGameBuilder(this).build();
            playerFactory = new PlayerFactory(ruleController, heapProxy, cardPack);
        }

        /* Players */

        private Builder addPlayer(PlayerControllerInterface player) {
            if (players.contains(player))
                throw new DuplicatePlayerException("Well, no. You cannot play twice.");
            if (players.size() >= MAX_PLAYERS)
                throw new InvalidPlayersNumberException("Too many  players... Join later.");
            players.add(player);
            return this;
        }

        @Override
        public Builder addPlayerLocal(String nick) {
            MePlayerUserInterface mePlayerUserInterface = gameUI.getMePlayerUserInterface();
            PlayerLocal player = (PlayerLocal) playerFactory.create(nick, PlayerLocal.class, mePlayerUserInterface);
            player.addObserver(mePlayerUserInterface);
            mePlayerUserInterface.update(player, null);
            return addPlayer(player);
        }

        @Override
        public Builder addPlayerCPU(String nick) {
            PlayerUserInterface playerUserInterface = gameUI.getPlayerUserInterface();
            PlayerCpu player = (PlayerCpu) playerFactory.create(nick, PlayerCpu.class, playerUserInterface);
            player.addObserver(playerUserInterface);
            playerUserInterface.update(player, null);
            return addPlayer(player);
        }

        /* Builder */

        private GameEngine game;

        @Override
        public GameEngine build() {
            if (game != null)
                return game;

            if (players.size() < MIN_PLAYERS)
                throw new InvalidPlayersNumberException("Oh gosh, this is not solitaire.");

            heap.build();

            game = new GameEngine(new ArrayList<>(players));
            game.addObserver(gameUI.getHeapUserInterface());
            return game;
        }
    }
}
