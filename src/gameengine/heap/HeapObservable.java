package gameengine.heap;

import gameengine.cards.CardInterface;
import gameengine.exceptions.NoCardsException;

import java.util.Observable;
import java.util.Observer;

/**
 * Represents non-shuffle-able heap, queue implementation
 * This one is Observable, too!
 *
 * @author Jiří Lajčok
 */
public final class HeapObservable extends Observable implements HeapInterface, Observer {

    /* References */

    private final HeapInterface heap;

    /* Initialization */

    public HeapObservable(HeapInterface heap) {
        this.heap = heap;
        if (heap instanceof Observable)
            ((Observable) heap).addObserver(this);
        setChanged();
    }

    /* Observer */

    @Override
    public void update(Observable o, Object arg) {
        setChanged();
        notifyObservers(arg);
    }

    /* Heap */

    @Override
    public CardInterface getTopCard() {
        return heap.getTopCard();
    }

    @Override
    public void putCard(CardInterface card) {
        heap.putCard(card);
        setChanged();
        notifyObservers(card);
    }

    @Override
    public CardInterface takeCard() throws NoCardsException {
        setChanged();
        notifyObservers();
        return heap.takeCard();
    }

}
