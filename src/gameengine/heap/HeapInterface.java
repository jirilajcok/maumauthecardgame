package gameengine.heap;

import gameengine.cards.CardInterface;
import gameengine.exceptions.DuplicateCardException;
import gameengine.exceptions.NoCardsException;
import gameengine.exceptions.NoCardsRuntimeException;

/**
 * Represents cards in the game.
 * You put the cards on top of it and take cards from beneath.
 * Illegal moves are exposed here.
 *
 * @author Jiří Lajčok
 */
public interface HeapInterface {

    /**
     * Shows the card on the top of the heap
     *
     * @return Returns the card on the top
     * @throws NoCardsRuntimeException In case run out of cards (should not happen)
     */
    CardInterface getTopCard();

    /**
     * Card is put on the top
     *
     * @param card Card to put on the top
     * @throws DuplicateCardException In case the card is already present in heap
     */
    void putCard(CardInterface card);

    /**
     * Take a card from heap from beneath (that has been put into heap first)
     *
     * @return Returns taken card
     * @throws NoCardsException In case there is only last card in heap (the top one)
     */
    CardInterface takeCard() throws NoCardsException;

}
