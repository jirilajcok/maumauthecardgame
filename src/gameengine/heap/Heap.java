package gameengine.heap;

import gameengine.cards.CardInterface;
import gameengine.cards.CardPackInterface;
import gameengine.exceptions.NoCardsException;
import gameengine.exceptions.NoCardsRuntimeException;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Represents non-shuffle-able heap, queue implementation
 *
 * @author Jiří Lajčok
 */
public final class Heap implements HeapInterface {

    /* Properties */

    private final Deque<CardInterface> heap;

    /* Initialization */

    /**
     * Fills heap with cards pulled from pack
     *
     * @param cardPack Pack to pull from
     * @throws NoCardsRuntimeException In case card pack is empty
     */
    Heap(CardPackInterface cardPack) {
        try {
            heap = new ArrayDeque<>(cardPack.getCards());
        } catch (NoCardsException e) {
            throw new NoCardsRuntimeException(e.getMessage() + " -- said card pack");
        }
    }

    /* Heap */

    @Override
    public CardInterface getTopCard() {
        if (heap.isEmpty())
            throw new NoCardsRuntimeException("Somebody has stolen all of heap!");

        return heap.peekLast();
    }

    @Override
    public void putCard(CardInterface card) {
        heap.offerLast(card);
    }

    @Override
    public CardInterface takeCard() throws NoCardsException {
        if (heap.size() <= 1)
            throw new NoCardsException("Wow, some player must have been packed up really well that there is no card left!");
        return heap.pollFirst();
    }

}
