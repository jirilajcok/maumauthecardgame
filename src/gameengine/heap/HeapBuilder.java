package gameengine.heap;

import gameengine.cards.CardInterface;
import gameengine.cards.CardPack;
import gameengine.cards.CardPackInterface;
import gameengine.exceptions.NoCardsException;

import java.util.Observable;

/**
 * Use this class to initiate heap without taking any cards from pack.
 * Ÿou can pass this as reference
 */
public final class HeapBuilder extends Observable implements HeapBuilderAgentInterface {

    /* References */

    private CardPackInterface cardPack;
    private HeapInterface heap;

    /* Initialization */

    public HeapBuilder() {
        cardPack = null;
    }

    public HeapBuilder(CardPackInterface cardPack) {
        this.cardPack = cardPack;
    }

    /* Builder */

    @Override
    public HeapBuilder setCardPack(CardPack cardPack) {
        this.cardPack = cardPack;
        return this;
    }

    @Override
    public HeapInterface build() {
        if (heap == null) {
            if (cardPack == null)
                throw new IllegalStateException("Card pack must be set before building a heap");
            heap = new Heap(cardPack);
            setChanged();
            notifyObservers();
        }
        return heap;
    }

    /* Heap */

    private HeapInterface getHeap() {
        if (heap == null)
            throw new IllegalStateException("Aye, heap is not filled with cards, yet. Try to build one first.");
        return heap;
    }

    @Override
    public CardInterface getTopCard() {
        return getHeap().getTopCard();
    }

    @Override
    public void putCard(CardInterface card) {
        getHeap().putCard(card);
    }

    @Override
    public CardInterface takeCard() throws NoCardsException {
        return getHeap().takeCard();
    }

}
