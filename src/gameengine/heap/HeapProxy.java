package gameengine.heap;

import gameengine.cards.CardInterface;
import gameengine.exceptions.NoCardsException;

public final class HeapProxy implements HeapInterface {

    /* References */

    private HeapInterface heap;

    /* Initialization */

    public HeapProxy(HeapInterface heap) {
        this.heap = heap;
    }

    /* Heap */

    @Override
    public CardInterface getTopCard() {
        return heap.getTopCard();
    }

    @Override
    public void putCard(CardInterface card) {
        heap.putCard(card);
    }

    @Override
    public CardInterface takeCard() throws NoCardsException {
        return heap.takeCard();
    }
}
