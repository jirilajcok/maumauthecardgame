package gameengine.heap;

import gameengine.cards.CardPack;
import javafx.util.Builder;

/**
 * Collects card pack and eventually builds heap
 *
 * @author Jiří Lajčok
 */
public interface HeapBuilderInterface extends Builder<HeapInterface> {

    /**
     * Sets card pack which is used to build a heap
     *
     * @param cardPack A card pack
     * @return Returns self
     */
    HeapBuilderInterface setCardPack(CardPack cardPack);

    /**
     * Builds heap based on given card pack
     *
     * @return Returns new instance of given heap type or one that has been already built
     * @throws IllegalStateException In case card pack has not been set, yet
     */
    @Override
    HeapInterface build();

}
