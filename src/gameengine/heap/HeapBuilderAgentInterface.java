package gameengine.heap;

/**
 * Connection of builder and basic heap used represents non-built heaps that can be already referenced.
 * If not built, actions are not successful but after being built, this is complete representative to heap.
 *
 * @author Jiří Lajčok
 */
public interface HeapBuilderAgentInterface extends HeapInterface, HeapBuilderInterface {
}
