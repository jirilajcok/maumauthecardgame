package gameengine.rules;

import gameengine.cards.CardInterface;

import java.util.Arrays;
import java.util.List;

/**
 * Represents rule set module manager
 *
 * @author Jiří Lajčok
 */
final class RuleComposite implements RuleInterface {

    /* Properties */

    private final List<RuleInterface> rules;

    /**
     * Adds a rule to list
     *
     * @param rule Rules to add
     * @return Returns self
     */
    public RuleComposite addRule(RuleInterface rule, RuleInterface... others) {
        rules.add(rule);
        rules.addAll(Arrays.asList(others));
        return this;
    }

    /* Initialization */

    /**
     * Initiate an object with rules
     *
     * @param rules Rules to initiate with
     */
    RuleComposite(RuleInterface... rules) {
        this.rules = Arrays.asList(rules);
    }

    /* Rule */

    @Override
    public boolean isStopped() {
        for (RuleInterface rule : rules)
            if (rule.isStopped())
                return true;
        return false;
    }

    @Override
    public int getCardsTake() {
        int max = 0;
        for (RuleInterface rule : rules) {
            int take = rule.getCardsTake();
            if (take > max)
                max = take;
        }
        return max;
    }

    @Override
    public boolean canPutCard(CardInterface card) {
        for (RuleInterface rule : rules)
            if (!rule.canPutCard(card))
                return false;
        return true;
    }

    @Override
    public int getSuitChange() {
        for (RuleInterface rule : rules) {
            int suit = rule.getSuitChange();
            if (suit > 0) return suit;
        }
        return 0;
    }

    @Override
    public void setSuitChange(int suit) throws IllegalStateException {
        rules.forEach(rule -> rule.setSuitChange(suit));
    }

    /* Controller */

    @Override
    public void reset() {
        rules.forEach(RuleInterface::reset);
    }

    @Override
    public void updateCard(CardInterface card) {
        rules.forEach(rule -> rule.updateCard(card));
    }
}
