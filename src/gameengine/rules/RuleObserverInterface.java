package gameengine.rules;

import java.util.Observer;

/**
 * Joins rules and observer of heap interface
 *
 * @author Jiří Lajčok
 */
public interface RuleObserverInterface extends RuleInterface, Observer {
}
