package gameengine.rules;

import gameengine.cards.CardInterface;
import gameengine.exceptions.IllegalGameStateException;
import gameengine.heap.HeapInterface;

import java.util.Observable;
import java.util.Observer;

public final class RuleController implements RuleInterface, Observer {

    /* Observer */

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof HeapInterface)
            updateCard(((HeapInterface) o).getTopCard());
        else if (arg instanceof CardInterface)
            updateCard((CardInterface) arg);
        else
            reset();
    }

    /* Rule */

    @Override
    public int getCardsTake() {
        return rules.getCardsTake();
    }

    @Override
    public boolean canPutCard(CardInterface card) {
        if (getSuitChange() == 0) {
            System.out.println("Rules: " + card);
            return rules.canPutCard(card);
        } else {
            System.out.println("Change: " + card);
            System.out.println("Change: " + getSuitChange());
            return change.canPutCard(card);
        }
    }

    @Override
    public int getSuitChange() {
        return change.getSuitChange();
    }

    @Override
    public void setSuitChange(int suit) {
        change.setSuitChange(suit);
    }

    @Override
    public boolean isStopped() {
        return stop.isStopped();
    }

    @Override
    public void reset() {
        System.out.println("RESET!");
        rules.reset();
    }

    @Override
    public void updateCard(CardInterface card) {
        System.out.println("Latest card: " + card);
        allRules.updateCard(card);
    }

    /* Rule Modules */

    private final RuleInterface general = new RuleInterface() {
        private CardInterface top;

        @Override
        public void updateCard(CardInterface card) {
            top = card;
        }

        @Override
        public int getCardsTake() {
            return 1;
        }

        @Override
        public boolean canPutCard(CardInterface card) {
            return card.equals(CardInterface.JACK) || top.equals(card.getSuit()) || top.equals(card.getValue());
        }
    };
    private final RuleInterface stop = new RuleInterface() {
        private boolean stop;

        @Override
        public boolean isStopped() {
            return stop;
        }

        @Override
        public void reset() {
            stop = false;
        }

        @Override
        public void updateCard(CardInterface card) {
            stop = card.equals(CardInterface.ACE);
        }

        @Override
        public boolean canPutCard(CardInterface card) {
            return !stop || card.equals(CardInterface.ACE);
        }
    };
    private final RuleInterface take = new RuleInterface() {
        private int take;

        @Override
        public void reset() {
            take = 0;
        }

        @Override
        public void updateCard(CardInterface card) {
            if (card.equals(CardInterface.SUIT_LEAVES, CardInterface.KING)) {
                take += 4;
            } else if (card.equals(7)) {
                take += 2;
            }
        }

        @Override
        public int getCardsTake() {
            return take;
        }

        @Override
        public boolean canPutCard(CardInterface card) {
            return take == 0 || card.equals(7) || card.equals(CardInterface.SUIT_LEAVES, CardInterface.KING);
        }
    };
    private final RuleInterface change = new RuleInterface() {
        private boolean expect;
        private int suit;

        @Override
        public void reset() {
            expect = false;
            suit = 0;
        }

        @Override
        public void updateCard(CardInterface card) {
            expect = card.equals(CardInterface.JACK);
            suit = 0;
        }

        @Override
        public int getSuitChange() {
            return suit;
        }

        @Override
        public void setSuitChange(int suit) {
            if (!expect)
                throw new IllegalGameStateException("Cheater! You trying to change suit with normie card?");

            if (suit != 0 && (suit < CardInterface.SUIT_MIN || suit > CardInterface.SUIT_MAX))
                throw new IllegalArgumentException("What the suit?!"); // TODO: Change to NoSuchCardException?

            expect = false;
            this.suit = suit;
        }

        @Override
        public boolean canPutCard(CardInterface card) {
            return suit == 0 || card.equals(CardInterface.JACK) || card.equals(suit);
        }
    };
    private final RuleInterface rules = new RuleComposite(general, stop, take);
    private final RuleInterface allRules = new RuleComposite(rules, change);
}
