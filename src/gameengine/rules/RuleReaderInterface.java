package gameengine.rules;

import gameengine.cards.CardInterface;
import gameengine.exceptions.IllegalGameStateException;
import gameengine.player.PlayerInterface;

public interface RuleReaderInterface {

    /* Static */

    /**
     * Initial number of player's cards
     */
    int PLAYER_CARDS = 4;

    /**
     * Determines whether a player has won (has no cards left)
     *
     * @param player A player to be checked
     * @return Returns whether it is a win
     */
    static boolean hasWon(PlayerInterface player) {
        return player.getNumberOfCards() <= 0;
    }

    /* Interface */

    /**
     * Determines whether game is stopped
     *
     * @return Returns whether the player is stopped
     */
    default boolean isStopped() {
        return false;
    }

    /**
     * Get number of cards to take if there are some,
     * might be 0 e.g. if the game is stopped
     *
     * @return Number of cards to take according to the specific rule
     */
    default int getCardsTake() {
        return 0;
    }

    /**
     * Find out if move is legal (particular card can be put)
     * or even whether card is expected at all (e.g. turn is stopped)
     *
     * @param card Card to be put
     * @return Returns whether the move is legal
     */
    default boolean canPutCard(CardInterface card) {
        return true;
    }

    /**
     * What has been suit changed to?
     *
     * @return Returns the suit it has been changed to or 0 for default suit (on card)
     */
    default int getSuitChange() {
        return 0;
    }

    /**
     * Resets status, usually used when card is taken, player halted or game is initiating
     *
     * @throws IllegalGameStateException In case status has not been changed or reset is disabled at the moment
     */
    default void reset() {

    }

}
