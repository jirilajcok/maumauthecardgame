package gameengine.rules;

import gameengine.cards.CardInterface;
import gameengine.exceptions.IllegalGameStateException;

/**
 * Represents any rule of the game to be controlled.
 *
 * @author Jiří Lajčok
 */
public interface RuleInterface extends RuleReaderInterface {

    /**
     * Top card on which the state depends and changes
     *
     * @param card Card to update status according to
     */
    void updateCard(CardInterface card);

    /**
     * Set-up suit change if needed.
     * Use this when choose suit exception triggered to set it up.
     *
     * @param suit Suit to be changed to. 0 = no suit change (default suit)
     * @throws IllegalGameStateException In case change request is not emitted
     */
    default void setSuitChange(int suit) {
    }

}
