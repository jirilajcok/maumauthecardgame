package gameengine.rules;

import gameengine.cards.CardInterface;

public final class RuleProxy implements RuleReaderInterface {

    /* Properties */

    private final RuleReaderInterface rule;

    /* Initialization */

    public RuleProxy(RuleReaderInterface rule) {
        this.rule = rule;
    }

    /* Rule */

    @Override
    public boolean isStopped() {
        return rule.isStopped();
    }

    @Override
    public int getCardsTake() {
        return rule.getCardsTake();
    }

    @Override
    public boolean canPutCard(CardInterface card) {
        return rule.canPutCard(card);
    }

    @Override
    public int getSuitChange() {
        return rule.getSuitChange();
    }

    @Override
    public void reset() {
        rule.reset();
    }
}
