package gui.exceptions;

public final class NoSuchImageException extends GameRuntimeException {

    public NoSuchImageException(String message) {
        super(message);
    }

}
