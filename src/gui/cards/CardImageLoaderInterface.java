package gui.cards;

import gameengine.cards.CardInterface;
import javafx.scene.image.Image;

/**
 * Loader of card images
 *
 * @author Jiří Lajčok
 */
public interface CardImageLoaderInterface {

    /**
     * Loads an image based on card itself
     *
     * @param card A card
     * @return Returns an image of card
     */
    Image getImage(CardInterface card);

    /**
     * Loads an image of back of unknown card
     *
     * @return Returns an image of unknown card back
     */
    Image getImage();

}
