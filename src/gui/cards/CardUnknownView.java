package gui.cards;

import javafx.scene.image.ImageView;

public final class CardUnknownView extends ImageView {

    /* Initialization */

    public CardUnknownView() {
        super(CardImageLazyLoader.getInstance().getImage());
    }

}
