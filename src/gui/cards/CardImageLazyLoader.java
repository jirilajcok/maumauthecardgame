package gui.cards;

import gameengine.cards.CardInterface;
import gui.images.ImageLoader;
import gui.images.ImageLoaderInterface;
import javafx.scene.image.Image;

public final class CardImageLazyLoader implements CardImageLoaderInterface {

    /* Constants */

    private static final int SUITS = CardInterface.SUIT_MAX - CardInterface.SUIT_MIN + 1;
    private static final int VALUES = CardInterface.ACE - CardInterface.SEVEN + 1;

    private static final String[] SUIT_NAMES = {"Clubs", "Spades", "Hearts", "Diamonds"};
    private static final String[] VALUE_NAMES = {"7", "8", "9", "10", "J", "Q", "K", "A"};

    /* Properties */

    private final Image[][] cache = new Image[SUITS][VALUES];
    private Image unknown;

    /* References */

    private final ImageLoaderInterface loader = new ImageLoader("src/gui/cards/img");

    /* Initialization */

    private CardImageLazyLoader() {
    }

    /* Singleton */

    private static CardImageLazyLoader instance;

    public static CardImageLazyLoader getInstance() {
        if (instance == null)
            instance = new CardImageLazyLoader();
        return instance;
    }

    /* Loader */

    @Override
    public Image getImage(CardInterface card) {
        int suit = card.getSuit() - CardInterface.SUIT_MIN;
        int value = card.getValue() - CardInterface.SEVEN;

        if (cache[suit][value] != null)
            return cache[suit][value];

        String imageName = "card" + SUIT_NAMES[suit] + VALUE_NAMES[value] + ".png";
        return cache[suit][value] = loader.getImage(imageName);
    }

    @Override
    public Image getImage() {
        if (unknown == null)
            unknown = loader.getImage("cardBack.png");
        return unknown;
    }
}