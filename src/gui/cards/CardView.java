package gui.cards;

import gameengine.cards.CardInterface;
import javafx.scene.image.ImageView;

public final class CardView extends ImageView implements CardInterface {

    /* References */

    private final CardInterface card;

    /* Initialization */

    public CardView(CardInterface card) {
        super(CardImageLazyLoader.getInstance().getImage(card));
        this.card = card;
    }

    /* Card */

    @Override
    public int getSuit() {
        return card.getSuit();
    }

    @Override
    public int getValue() {
        return card.getValue();
    }

    @Override
    public boolean equals(int suitOrValue) {
        return card.equals(suitOrValue);
    }

    @Override
    public boolean equals(int suit, int value) {
        return card.equals(suit, value);
    }

    @Override
    public boolean equals(CardInterface card) {
        return this.card.equals(card);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CardInterface)
            return equals((CardInterface) obj);
        else if (obj instanceof Integer)
            return equals((int) obj);
        return false;
    }

    @Override
    public String toString() {
        return card.toString();
    }
}
