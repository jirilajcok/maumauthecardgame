package gui;

import gameengine.GameBuilderInterface;
import gameengine.GameInterface;
import gameengine.cards.CardInterface;
import gameengine.exceptions.InvalidPlayersNumberException;
import gameengine.heap.HeapInterface;
import gameengine.player.MePlayerInterface;
import gameengine.player.PlayerControllerInterface;
import gameengine.player.PlayerInterface;
import gameengine.rules.RuleReaderInterface;
import gameengine.ui.*;
import gui.cards.CardImageLazyLoader;
import gui.cards.CardImageLoaderInterface;
import gui.cards.CardUnknownView;
import gui.cards.CardView;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.*;

public final class TableController implements GameUserInterface, GameUserInterfaceBuilder, Initializable {

    /* Properties */

    @FXML
    private ImageView heapTopCard;
    @FXML
    private ImageView heapPack;
    @FXML
    private Label packText;
    @FXML
    private Label takeCards;

    @FXML
    private Label suitAcorns;
    @FXML
    private Label suitLeaves;
    @FXML
    private Label suitHearts;
    @FXML
    private Label suitBells;

    @FXML
    private HBox playerTop;
    @FXML
    private Label playerTopNick;
    @FXML
    private HBox playerBottom;
    @FXML
    private Label playerBottomNick;
//    @FXML
//    private VBox playerLeft;
//    @FXML
//    private VBox playerRight;

    /* References */

    private final CardImageLoaderInterface imageLoader = CardImageLazyLoader.getInstance();

    private HeapInterface heap;
    private RuleReaderInterface rules;
    private MePlayerInterface mePlayer;
    private GameBuilderInterface gameBuilder;
    private GameInterface game;

    private final Map<String, Pane> spots = new HashMap<>(4);
    private final Queue<Pane> spotOrder = new ArrayDeque<>();

    private final Map<String, Label> spotLabels = new HashMap<>(4);
    private final Queue<Label> labelOrder = new ArrayDeque<>();

    private final List<Label> suits = new ArrayList<>(4);

    private PlayerInterface currentPlayer;

    /* Initialization */

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        spotOrder.add(playerBottom);
        spotOrder.add(playerTop);
//        spotOrder.add(playerLeft);
//        spotOrder.add(playerRight);

        labelOrder.add(playerBottomNick);
        labelOrder.add(playerTopNick);

        suits.add(suitAcorns);
        suits.add(suitLeaves);
        suits.add(suitHearts);
        suits.add(suitBells);

        for (Label suit : suits)
            suit.setVisible(false);
    }

    /* Builder */

    @Override
    public TableController setRules(RuleReaderInterface rules) {
        this.rules = rules;
        return this;
    }

    @Override
    public TableController setHeap(HeapInterface heap) {
        this.heap = heap;
        if (heap instanceof Observable)
            ((Observable) heap).addObserver(getHeapUserInterface());
        return this;
    }

    @Override
    public TableController setMePlayer(MePlayerInterface mePlayer) {
        this.mePlayer = mePlayer;
        return this;
    }

    @Override
    public TableController setGameBuilder(GameBuilderInterface gameBuilder) {
        this.gameBuilder = gameBuilder;
        return this;
    }

    @Override
    public TableController build() {
        return this;
    }

    /* Controller */

    private GameInterface getGame() {
        if (game == null)
            game = gameBuilder.build();
        return game;
    }

    private Pane getPlayerSpot(PlayerInterface player) {
        if (!spots.containsKey(player.getNick())) {
            if (spotOrder.isEmpty())
                throw new InvalidPlayersNumberException("Too many players, join next time.");
            spots.put(player.getNick(), spotOrder.remove());

            Label label = labelOrder.remove();
            label.setText(player.getNick());
            spotLabels.put(player.getNick(), label);
        }
        return spots.get(player.getNick());
    }

    private Pane getPlayerSpot() {
        return getPlayerSpot(currentPlayer);
    }

    @FXML
    private void onMouseClicked(MouseEvent event) {
        if (currentPlayer != mePlayer)
            return;

        EventTarget target = event.getTarget();
        if (target == heapPack) {
            if (rules.isStopped())
                mePlayer.halt();
            else
                mePlayer.takeCards();
            return;
        } else if (target instanceof CardInterface) {
            mePlayer.putCard((CardInterface) target);
            return;
        }

//        System.out.println(target);
//        if (suits.contains(target)) {
//            int suit = suits.indexOf(target);
//            System.out.println("CHOSEN SUIT " + suit);
//            mePlayer.setSuitChange(suit);
//        }
    }

    /* User Interface */

    private final HeapUserInterface heapUI = new HeapUserInterface() {
        private void updatePackStatus() {
            if (rules.isStopped()) {
                packText.setText("Halt");
                takeCards.setText("");
            } else {
                packText.setText("Cards to take");
                takeCards.setText(String.valueOf(rules.getCardsTake()));
            }
        }

        private void updateSuitChange() {
            int suit = rules.getSuitChange();
            if (suit == 0)
                for (Label suitLabel : suits)
                    suitLabel.setVisible(false);
            else
                for (int i = 0; i < suits.size(); i++)
                    suits.get(i).setVisible(suit == i + 1);
        }

        @Override
        public void update(Observable o, Object arg) {
            heapTopCard.setImage(imageLoader.getImage(heap.getTopCard()));
            heapPack.setImage(imageLoader.getImage());
            updatePackStatus();
            updateSuitChange();
        }
    };

    private final PlayerUserInterface playerUI = new PlayerUserInterface() {
        private void init(PlayerInterface player) {
            List<Node> cards = getPlayerSpot().getChildren();
            cards.clear();
            for (int i = 0; i < player.getNumberOfCards(); i++)
                cards.add(new CardUnknownView());
        }

        @Override
        public void turn(PlayerInterface player) {
            currentPlayer = player;
        }

        @Override
        public void cardPut(CardInterface card) {
            List<Node> cards = getPlayerSpot().getChildren();
            if (!cards.isEmpty())
                cards.remove(cards.size() - 1);

            if (!card.equals(CardInterface.JACK))
                getGame().turn();
        }

        @Override
        public void cardsTaken(int amount) {
            List<Node> cards = getPlayerSpot().getChildren();
            for (int i = 0; i < amount; i++)
                cards.add(new CardUnknownView());
            getGame().turn();
        }

        @Override
        public void halted() {
            getGame().turn();
        }

        @Override
        public void suitChosen(int suit) {
            game.turn();
        }

        @Override
        public void update(Observable o, Object arg) {
            PlayerInterface player;
            if (o instanceof PlayerInterface)
                player = (PlayerInterface) o;
            else if (o instanceof PlayerControllerInterface)
                player = ((PlayerControllerInterface) o).getPlayer();
            else return;

            turn(player);

            if (arg instanceof CardInterface)
                cardPut((CardInterface) arg);
            else if (arg instanceof Integer)
                cardsTaken((int) arg);
            else if ("h".equals(arg))
                halted();
            else if ("s".equals(arg))
                suitChosen(rules.getSuitChange());
            else
                init(player);
        }
    };

    private final MePlayerUserInterface mePlayerUI = new MePlayerUserInterface() {
        private void init(MePlayerInterface player) {
            List<Node> cards = getPlayerSpot().getChildren();
            cards.clear();
            for (CardInterface card : (player).getCards())
                cards.add(new CardView(card));
        }

        @Override
        public void turn(PlayerInterface player) {
            if (player != mePlayer) return;
            currentPlayer = player;
        }

        @Override
        public void cardPut(CardInterface card) {
            getPlayerSpot().getChildren().remove(card);
//            if (card.equals(CardInterface.JACK))
//                chooseSuit();
//            else
            getGame().turn();
        }

        @Override
        public void cardsTaken(int amount) {
            init(mePlayer);
            getGame().turn();
        }

        @Override
        public void halted() {
            getGame().turn();
        }

        @Override
        public void chooseSuit() {
            for (Label suitLabel : suits)
                suitLabel.setVisible(true);
        }

        @Override
        public void suitChosen(int suit) {
            getGame().turn();
        }

        @Override
        public void update(Observable o, Object arg) {
            MePlayerInterface player;
            if (o instanceof MePlayerInterface && mePlayer == null)
                mePlayer = (MePlayerInterface) o;
            if (o.equals(mePlayer))
                player = mePlayer;
            else if (o instanceof PlayerControllerInterface && arg == mePlayer)
                player = mePlayer;
            else return;

            turn(player);

            if (arg instanceof CardInterface)
                cardPut((CardInterface) arg);
            else if (arg instanceof Integer)
                cardsTaken((int) arg);
            else if ("h".equals(arg))
                halted();
//            else if ("s".equals(arg))
//                suitChosen(rules.getSuitChange());
            else
                init(player);
        }
    };

    @Override
    public HeapUserInterface getHeapUserInterface() {
        return heapUI;
    }

    @Override
    public PlayerUserInterface getPlayerUserInterface() {
        return playerUI;
    }

    @Override
    public MePlayerUserInterface getMePlayerUserInterface() {
        return mePlayerUI;
    }

}
