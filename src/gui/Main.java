package gui;

import gameengine.GameEngine;
import gameengine.GameInterface;
import gameengine.ui.GameUserInterfaceBuilder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream("table.fxml"));
        primaryStage.setTitle("Mau Mau: The Card Game");
        primaryStage.setScene(new Scene(root, 800, 680));

        GameUserInterfaceBuilder ui;
        ui = loader.getController();

        GameInterface game = (new GameEngine.Builder(ui))
                .addPlayerLocal("Local")
                .addPlayerCPU("Bot")
                .build();

        primaryStage.show();

        game.turn();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
