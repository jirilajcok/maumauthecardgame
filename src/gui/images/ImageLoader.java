package gui.images;

import javafx.scene.image.Image;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class ImageLoader implements ImageLoaderInterface {

    /* Properties */

    private final Path dir;

    /* Initialization */

    public ImageLoader(String dir) {
        try {
            this.dir = Paths.get(dir);
        } catch (InvalidPathException e) {
            throw new IllegalArgumentException("Given path" + dir + "is invalid.");
        }

        if (!Files.isDirectory(this.dir))
            throw new IllegalArgumentException("Given path " + this.dir + " is not a directory.");
    }

    /* Loader */

    @Override
    public Image getImage(String imageName) {
        Path path = Paths.get(dir.toString(), imageName);
        return new Image(path.toUri().toString());
    }
}
