package gui.images;

import gui.exceptions.NoSuchImageException;
import javafx.scene.image.Image;

/**
 * Image lazy loading and caching
 *
 * @author Jiří Lajčok
 */
public interface ImageLoaderInterface {

    /**
     * Gets image based on its name
     *
     * @param imageName Image name to get
     * @return Returns requested image
     * @throws NoSuchImageException In case image was not found
     */
    Image getImage(String imageName);

}
